import { all, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';

import { User } from 'types';
import UserActions, { UserTypes } from '../redux/users';

function* getUsers(): any {
  const { data } = yield axios.get('https://jsonplaceholder.typicode.com/users');
  const users = yield data.map((user: any) => user as User);
  yield put( UserActions.setUsers(users));
}

export default function* root() {
  yield all([takeLatest(UserTypes.INIT_USERS, getUsers)]);
}
