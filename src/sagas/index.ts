/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { fork } from 'redux-saga/effects';

import AppSaga from './usersSaga';

export default function* root() {
  yield fork(AppSaga);

}
