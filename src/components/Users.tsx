import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import { User } from 'types';
import UserActions from '../redux/users';

type UserProps = {
  users: User[];
  userActions: any;
}

const Users = ({users, userActions}: UserProps) => {

  useEffect(() => {
    const fetchAllUsers = async () => {await userActions.initUsers()};
    fetchAllUsers();
  }, []);


  if (users && users.length === 0 ) return null;
  console.log(users);
  return (
  <table>
	
    <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>address</th>
          <th>company</th>
          <th>email</th>
          <th>phone</th>
          <th>username</th>
          <th>website</th>
        </tr>
    </thead>
    <tbody style={{fontWeight: 'normal'}}>
      {users.map((user: User) => (
        <tr key={user.id}>
        <th>{user.id}</th>
          <td>{user.name}</td>
          <td>{Object.values(user.address)[0] as unknown as string}</td>
          <td>{Object.values(user.company)[0] as unknown as string}</td>
          <td>{user.email}</td>
          <td>{user.phone}</td>
          <td>{user.username}</td>
          <td>{user.website}</td>
      </tr>
      ))}
        
    </tbody>
    <caption style={{marginBottom: 50}}>Users from https://jsonplaceholder.typicode.com/users</caption>
  </table>);
};

const mapDispatchToProps = (dispatch: any) => ({
  userActions: bindActionCreators(UserActions, dispatch)
});

const mapStateToProps = (state: any) => ({
  users: state.users.users
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);

