import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import createStore from './store/createStore';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
// eslint-disable-next-line no-underscore-dangle
const initialState = (window as any).__INITIAL_STATE__;
export const store = createStore(initialState);


ReactDOM.render(
  <React.StrictMode>
    <App store={store}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
