import { createReducer, createActions } from 'reduxsauce';
import { User } from 'types';


/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  initUsers: [],
  setUsers: ['users'],
});

export const UserTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export type UsersState = {
  users: User[]
};
const INITIAL_STATE: UsersState = {
  users: []
};

// export const authSelector: (state: any) => AuthState = (state: any) =>
//   state.auth;

/* ------------- Reducer ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_USERS]: (state, { users }) => {
    console.log(state);
    return ({
    ...state,
    users
  })}
});
