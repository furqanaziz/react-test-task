import { connectRouter, RouterState } from 'connected-react-router';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

export const routerSelector = (state: any) => state.router as RouterState;

export const reducer = (history: any) => connectRouter(history);
