
import React from 'react';
import { Provider } from 'react-redux';

import Router from 'routes/Router';
import './styles/App.css';
// import { Logo } from './icons'

function App({store}: any) {
  return (
    <>
    <Provider store={store}>
      <Router />
    </Provider>
    </>
  );
}

export default App;
