export interface User {
  id: string;
  address?: any;
  company?: any;
  email: string;
  name: string;
  phone: string;
  username: string;
  website: string;
};
