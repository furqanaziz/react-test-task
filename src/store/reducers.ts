/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-var-requires */
import { combineReducers } from 'redux';

const makeRootReducer = (history: any) => combineReducers({
    users: require('../redux/users').reducer,
    router: require('../redux/router').reducer(history)
  });

export const injectReducer = (store: any, { key, reducer }: any) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;
  // eslint-disable-next-line no-param-reassign
  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
