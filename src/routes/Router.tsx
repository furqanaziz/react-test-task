import { Switch, Route } from 'react-router';

import React, { lazy, Suspense } from 'react';

import { ConnectedRouter } from 'connected-react-router';
import { connect } from 'react-redux';

import history from '../customHistory';

const Users = lazy(() => import('../components/Users'));


const retry: any = (
  fn: Function,
  retriesLeft: any = 5,
  interval: any = 1000
) => 
  new Promise((resolve, reject) => {
    fn()
      .then(resolve)
      .catch(async (error: Error): Promise<void> => {
        await new Promise(resolve => setTimeout(resolve, interval));
        if (retriesLeft === 1) {
          window.location.reload(true);
        }
        retry(fn, retriesLeft - 1, interval).then(resolve, reject);
      }, interval);
  });

type RouterProps = {
  media?: string;
};

const Router: React.FC<RouterProps> = () => (
    <ConnectedRouter history={history}>     
        <Suspense fallback={<>Could not load Page</>}>
          <Switch>
            <Route exact path="/" render={() => <Users /> } />
          </Switch>
        </Suspense>
    </ConnectedRouter>
  );

const mapStateToProps = (state: any) => ({
  pathname: state.router.location.pathname
});

export default connect(mapStateToProps)(Router);
