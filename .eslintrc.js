module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    "extends": [
        "plugin:react/recommended",
        "airbnb-typescript",
        "plugin:@typescript-eslint/recommended",
        "prettier",

    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module",
        "project": ["./tsconfig.json"]
    },
    "plugins": [
        "react",
        "@typescript-eslint"
    ],
    "rules": {
        "import/prefer-default-export": "off",
        "@typescript-eslint/no-shadow": "off",
        "import/no-extraneous-dependencies": "off",
        '@typescript-eslint/no-var-requires': "off",
        "global-require": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "react/no-unused-prop-types": "warn",
        "@typescript-eslint/ban-types": "warn",
        "react/no-unused-prop-types": "warn"
    }
    
};
